**Attendees** ([click here for intros](https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/wiki/Group-Members---Bios)
* Andrew Lamb - Field Ready (group facilitator)
* Emily Cook - Standards Repo (secretariat)			
* Tom Bartley - Standards Rep	(technical authoring) 		
* Nathan Parker - makernet.work	
* Emily Cook - Standards Repo (secretariat)
* Nathan Parker - makernet.work
* Neil Noble - ex-Practical Action
* Jon Shull - Enable founder					
* Naiomi Lundman - Humanitarian Makers				
* Kaspar Emanuel - kitspace.org						
* Jeremy Stirling - Research into Open Source hardware.			
* Richard Hulskes - Co-founder Wivolver (
* Raya Sharbain - Makerspaces					
* Kshitiz Khanal - Kathmandu Living Labs						
* Jean-Michael  - Product manager Swiss NGO			
* Diderik van Wingerden - Independent consultant Think-Innovation.com
* Emilio Velis - Appropedia 
* Toby Wenzel - Co-founder Docubricks
* Alex Petsiuk - MIT scientist
* Julian Stirling - Uni of Bath (with RBowman)
* Brynmor John - Field Ready
* Jenny Molloy - University of Cambridge

**Apologies:**
* Anna Lowe - Manufacturing Change
* Richard Bowman - Uni of Bath
* Brian Garret - 3D Hubs
* Martin Haeuer - OSE Germany
* Michael Weinberg - OSHWA


-----



**Agenda:**
1. Introductions
2. Summarise previous call / Purpose
3. Shuttleworth Foundation Funding
4. Standards Repo
5. Warsaw Event
6. AOB
7. Next steps

**2. Summary/Purpose**
All attendees to come extent have been considering the issue of sharing documentation for making hardware products around the world. Many of you have platforms or approaches to try and standardise the way we publish OHW knowledge. We want to agree some sort of basic open hardware document which makes both sharing and finding resources easier, advancing the field and making sure we are all working the same way.

**3. Shuttleworth Foundation**
ALamb has managed to secure funding through the shuttleworth foundation to develop an open standard caller Open Know-How and under the makernet alliance. (SF supports open initiatives therefore is exceptionally aligned.) There are lots of things happening e.g the DIN standard and we are therefore hoping to work together as a community.

**4. Standards Repo**
Have been brought in by Andrew Lamb. SRepo was founded in late 2018.  We believe that a community-driven approach is how we get people involved. We believe in open, collaboration and achieving consensus but are not in the open hardware world. We have been brought in to help to develop the standard as external practitioners who have expertise in writing standards but are impartial in the Open Source Hardware world. We are a software company who have a git-based platform with version control. We have been doing background reading about your work and how you document hardware.  We will be reaching out to reach out to people about what people are working on and what is happening and capture this.. The SRepo platform will host most of the conversations for this working group. 
The aim: what will the world look like once the standard has been implemented. Initially we will focus on what outcomes we would like to achieve, which is discoverability for designers publishing, and makers finding OSH content. We are aware of Bath university and Docubricks etc and so we will take you through a lightweight approach in developing the standard. 

**5. The event:** 
There will be an event in Warsaw 11th-12th July, enabled by the Shuttleworth funding. This will sit alongside an event on the internet of Manufacturing earlier the same week. We are not able to invite everyone to this event so have spoken to some key people however, we will be able to engage over calls and using the Standards Repo platform so all of the working group are invited to contribute. The event will invite those who have developed their understanding of what an Open Hardware documentation standard might be. The outcome of this event will give a draft standard which we can then explore adoption in the future across different platforms. There will be a funded process to engage documentation sharing platforms (How this moves forward has not been decided yet, it could be consultants integrating the standards of funding available for teams to do so internally. Then there will potentially be tools funded for organisations to convert the documentation itself into something which can be made portable and findable using the standard.)
(Unfortunately there is limited finding so not all members will be invited to the even in person. Hopefully Standards Repo will support an accelerated process for this piece of work, and will be coordinating this work to ensure we get what we need.)

**6. AOB**
* Tom Bartley shared the Wiki for future reference.
* Tobey Wenzel - when we set out to create DocuBricks, we hoped that in the open source spirit others in the community would contribute/fork/further develop as well, which only happened rarely outside the core DocuBricks team. When extending a standard to include more information and other representation formats, feel free to build on and extend DocuBricks itself.
* DIN - Jeremy updated on the DIN standard. Jeremy is coordinating this initiative with Martin. The First version for comment will be published at the end of the summer with final version by the end of the year. It consists of three documents:
    * Defining Open Source Hardware so we know what constitutes this/what requirements are there so you can call it this.
    * Documents outlining a peer review/certification procedure
    * Guidelines
Jeremy confirmed that there are copyright issues and so will leave to Martin to share.  ALamb has secured legal support through the Shuttleworth Foundation based in Washington who knows IP, patenting etc which you could access if needed. The DIN standard they are editing is an extension of the OSH definition, and they considered licensing terms defining the terms. In the standard, they rquire more detail about the content to share, and less about the licensing terms of the informations. i.e more about content but less about license. 
ALamb understands that the OSHWA process doesn't specify how it is docuemnted. To avoid the pitfalls of the OSH specification the definition is about licensing terms. DIN continues where this stops. ALamb - this leaves lots of open ways to adopt the standard in the future and it is an exciting contribution to this space.
* Jeremy - a project starting in September will work with software which  will set up a webcrawler which might be able to trawl the internet and collate information automatically. This could perhaps be integrated in this initiative. 

**7. Next steps:**

1. Next working group call proposed is June 27th. (Since amended to 1st July by ALamb)
2. Emily will [send a survey ](https://docs.google.com/forms/d/e/1FAIpQLScQmzIQtSbKDffDSIqxz4jkQrL3kRSVpWsulBAptAIapXP3Sg/viewform?usp=sf_link)to aid the process of discovery and to define some of the priorities of the standard which you can all forward to your peers. You can include any information such as screenshots etc in order to aid this process of discovery.
3. If you have more you would like to say, please [use this link](https://calendly.com/emily-sr/okhwg) to arrange a call with Tom at Standards Repo
4. By the end of next week we will have a scope which we will send round for review before the next call
