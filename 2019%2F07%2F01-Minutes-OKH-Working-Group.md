## **Attendees**

* Tom Bartley - Standards Repo (chair)
* Emily Cook - Standards Repo
* Andrew Lamb - Field Ready
* Andre Maia Chagas - Mozilla Fellow
* Nathan Parker - Makernet
* Emilio Velis - Appropedia
* Neil Noble - ex-Practical Action
* Diderik van Wingerden - Think-Innovation.com
* Raya Sharbain - Jordan Open Source Association / Azraq Alive Makerspace
* Balthas Siebold - GIZ
* Enrico Bassi	- Careables


## Apologies
* Tobias Wenzel - Docubricks
* Julian Stirling - University of Bath
* Michael Weinberg - OSHWA
* Brian Garret - 3D Hubs
* Tom Salfield - Wikifactory


## Agenda
* Recap of progress so far
* Proposed next steps
* Questions.


### Introduction
ALamb gave a brief intro into the project and explained the context of the working group in drafting the standard, alongside the event.

### Presentation
TBartley ran through [the presentation] (https://docs.google.com/presentation/d/1go9lnhHlG3umAOjpRFILk5PQltgm4UjRnH-UZRIibJU/edit#slide=id.g5c966ee747_0_24) & sent next steps. 

###Questions 
1. ALamb: Could you talk us through an example for a data standard?
TB walked the group through this spreadsheet and talked through the plan to go through each ‘platform’ to look at what data it requires to upload/host data.

2. ALamb: Manifest I see as a ship’s manifest. Are you using the same definition?
TB: Manifest is the same, the idea is to provide an index for the hardware and this is what we mean by manifest. 

3. ALowe: In the document you talk about a standard to be used for open Hardware. I’d also like for this to be a data structure that could be used for non-open hardware also. The standard would be open and agnostic bout what I{ structure is documented using it. 
TB: This is for the group to decide. It is indeed possible, but for the group to discuss.
ALamb: I’d like the documentation standard to pr applied to both. 

4. JSchull: Is there a comparable standard for Software or Meta-category that Software and hardware have in common? Does something already exist?
ALamb: The definition of open-source is hotly debated, and has been contested often. I think that there is an easy answer - in the open data movement (put fwd by open web foundation) about data being 1* or 5*.  There is a lot of parallel with the patent system. If you can begin to express a technique and it is unique it can be patented.  We are not trying to replicate something which exists, we know it is tacit and hard to define.
Michael Weinberg: The short answer is yes. It highlights the challenge of hardware versus software. With software you have source code and licence and this describes it all. With hardware the different info you need to convey, and the different formats it needs to take are a lot more varied. Software standards therefore don’t get you as far. With hardware the combination of documents varies significantly. 

5. EmilioV: My question is about what the main word for what do we will document is: project/thing/product, etc. I think that might be useful to do in Warsaw.
TB: great question. We talk about a ‘thing’ but semantics aside we agree on what we are doing and therefore precise terminology shouldn't matter. 
Martin: DIN spec uses the term’ piece of hardware’  as opposed to product or artefact.

6. Diderick: It’s great to see how you have come up with the scope and levels. I agree and hope we can move forward. Looking at the scope and as we are discussing which fields are mandatory and optional is important but I’d challenge you to see if we can also include two other aspects which I think are vital to success of this initiative. 
Can we look at a process to go from the specification 1.0 to 1.1and 1.2 so we have somewhere to continue with the project that is pre-agreed.
Can we get some major platforms behind these specifications and see if they can adopt the standard and adopt them in a first specification which is adoptable and then combine or link to creative commons index or searchable for a crawler etc. 
ALamb: I’d like to define something moving forward and could perhaps put more funding into collaborations (through Shuttleworth foundation) combined with other opportunities  such as through Bath University. At the gathering in Warsaw the 3rd session will be talking about this exact topic. 
TB: the guys at uncompromise will be facilitating this session so  I’m hesitant to heavily define this, but we will leverage working together. 
Key information will be held in the SRepo Wiki, and a key role for the working group will be key to its success, and we will be asking members to test out and compare their hardware to see if it would work with the standard. 
ALamb: Diderick I hope we can fund these further ideas. I know Julian and Richard at Bath University have some ideas on this. Last weekend there was a wikifactory hackathon where they were designing hardware for makers to support them to document making things. There are a lot of ideas here, I’d be open to paying platforms to make the changes they would need to adopt this standard. 

7. Kaspar: does the DIN go into more detail on BOM?
TBartley: In terms of level 1, the BOM should be answered by discoverability. Some people have ideas about links to products to source them locally, but we have not specifically looked into contents of BOM. 
ALamb: it is possible to have cascading BOM functions. If you need 3 nails for step one and 4 for step two, you need seven nails. But if you need a hammer for both it is just one hammer. 
MHauer: DIN is handling BOM (still in draft) the documentation of a piece of hardware strongly depends no the technology which is listed. Every documentation needs to list the documentation-specific technology, including a list of documents needed to deliver for that particular technology. ‘TSTC’ for complex technology you may need more than one TSTC. This may provide the fingerprint for a search engine to find the information
We are also thinking of the completeness of this certification. The documentation must be complete for a protoype that works, and then can provide maintenance, operation also. The technology ‘TSTC’ and which lifecycle phases of the documentation. (e.g maintenance and operation.)

8. Nathan: would like a clear summary outcome for next week in Warsaw.
ALamb: I think Tom is aiming to get to L1 discoverability by the end of August, and by the end of Warsaw, you will be facilitating a series of discussions agreeing level 1 definition.
TB: defining what the standard will do, and how it will do it = outcome. This will include technical conversations about recommended fields, handling translation, etc.  
The definition of what Level1 is will be the outcome of the workshops. 
NParker: This is helpful. If the idea is to move forward an ongoing project to reach a milestone this is easier to plan for and provides a realistic timeline for Warsaw. 
ALamb: if you aren;t able to get to Warsaw, we will keep you involved in the process regardless.
TBartley: in the scope of the workshop I propose we will have a parking wall where we document other non-relevant ideas for future discussion.

9. JSchull: I suggest that "Proper Use" be part of the standard.  Address the possibility of mis-use or inadequate followup.  Makers often fall in the love with the thing, and forget the purpose and the responsibility.


10. NNoble: How far do we go in providing in providing information about the process and assuming that person already knows. ALamb gave the example that in rural vanuatu don't know the word ‘pump’ but do know that they want to lift water. 