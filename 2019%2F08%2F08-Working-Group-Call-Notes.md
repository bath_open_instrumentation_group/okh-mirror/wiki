## Slideshow: 
https://docs.google.com/presentation/d/1Ktlu2kEsz7PVy2Rc6lDzlJtPT0UUQMay8kO9QUieskg/edit#slide=id.p

## Attendees:
* Andrew Lamb
* Tom Bartley
* Richard Bowman
* Julian Stirling
* Brynmoor John
* Kshitiz Khanal
* Emilio Velis
* Neil Noble

## Apologies: 
* Michael Weinberg
* Martin Hauer
* Anna Lowe

## Notes:
### Introduction
ALamb gave an introduction. This is the first call since Warsaw. The call will be in two parts, to review the standard which has been sent out for comment and to feedback on the Warsaw event. 

### Recap
TBartley talked through the slide deck, recapping on the project brief, the learning and the suggested implementation heading into Warsaw. The aim for the Warsaw event was to produce a specification for 'Level 1' discoverable Know-How. Prior to Warsaw there was a draft scoping document and a suggested approach to provide a frame of reference going into the Warsaw event.

### Event recap
The event delved into assumptions, hopes and fears and where the strenghs and weaknesses of the approach, as well as responsibility for adoption and implementation. Sessions included scoping and assumptions, crucially coming up with the idea that if we have a manifest listing all contents, what would be required to make this at all useful? What would we recommend to make it really valueable? We will not make a value judgement on if it was recommended or required. There prevailing list of essential criteria was as follows:
* Title
* Who produced the documentation
* Responsible party for manifest
* Description
* URL to either documentation or project page
* License so you know what permissions you have

There were a longer list of recommended fields, not in the metadata space, but what documentation we wanted to link to.

At openknowhow.org this content is available. The info pages contain background notes and documentation we have developed up to this stage, how to comment on the candidate and links to teh candidate specification. There is a link also to the slides from the Warsaw outcomes. 

### The Candidate
You can find this on the standard tab, where there is a version for comment which is being proposed as the version. The audience for this document are designers, a large number of people in the community are working in technical environments and technical knowledge that other audiences will not necessarily know it exists, but will be adopted by platforms to make information more accessible to them on their behalf. For other audiences who want to learn more about discovering know-how they will be able to use this document. 

The terms and definitions were unchallenged in Warsaw. Most importantly we use the word 'thing' rather than hardware. Section 4 introduces the concept of the manifest. we had dicsussions about what this means. The manifest defines a version of the thing. Each new variant should have a new version. This was the outcome of the in-depth discussion around versions. We discussed this beinga YAML file, with a declaration which the group decided was an important version part of the specification. Also added, if you are a platform, rather than producing documentation maybe we don't need a separate folder to contain this. (Inspired by Emilio's suggestion of the V-Card spec which uses micro-formats to embed spec into web pages. Therefore adding each of the fields as a class to the page. We aren't asking platforms to produce a new tool, but to introduce some more metadata into their pages, easing adoption.) 

We spent a lot of time talking about required and recommended fields. This was presented as required, recommended, permissive, but instead grouped by what the field is trying to do. Metadata, (or the properties of the thing) license information, links to documentation, language and translation. We have added rules e.g that at least one license is included. These are set against each field. 
We agreed that extending the manifest scheme is a permissable. I'm not sure if this is sufficient for extending the scheme and won't break for future versions. We agreed that we will not try and version-manage the manifest itself, but that the content is HTTP headers or the repository information should give enough information to understand the manifest version. 
4.12 is a template for the manifest, so that you don't need to read the whole spec, but copy and paste this, put into text editor, rules are written to the right hand side so it is easy to adopt, so not everyone needs to go to th source documentation. The manifest for the required and recommended fields isn't a long file. It doesn't disincentivise use, these are things people provide anyway and it maps well to other platforms.
The last bit of the spec is around governance and future contributions. There is a working group established but this will be resolved by the end of the month. 

This was a whirlwind tour of the spec. I hope it feels familiar and that there aren't any shocks. I think I have captured all that was discussed and agreed. If there are errors and omissions, I will talk you throuh the commenting. (instructions are also available online.)

At this stage you should suggest changes through proposals. Go to the proposal screen, create a new proposal and be specific about what changes you want to make and why it is important. As proposals start to get populated, the world will have access to the issue tracker list, if you have details you can join the conversation and respond to comments. If anyone wants authoring permissions you can ask for these. 
I hope this is clear, there is a lot of information, which is summarised on the entry page, openknowhow.org. 
I'd like all comments by 23rd August. 

I will reach out to people on a case-by-case basis for more comments, with a call w/c 26th August. 

### Questions:
Thank you for writing this up! Hopefully those who weren't able to attend the workshop can see the input reflected here. 
ALAmb: On the declaration of the manifest, 4.3.3, you mentioned that you'd had feedback from Emilio about the idea of how the declaration should work. Could you say more about that and what that might mean to the YAML file solution?
TB: Emilio's question was about how this could be encoded using existing methods. He mentioned other data formats. We agreed in Warsaw that usability and ease of writing is preferable to an XML file, as this is written by a person not a computer. The declaration is to say this is OKH manifest, with section 4.4 if you are not using a GIT repository and writing documentation by hand an alternative way of doing this is to embed metadata into the web page. (as vcard has been adopted by web pages)
ALamb: This is also supported by schema.org
Kaspar: Is there a link to where we can read more about that?
TBartley: The Wikipedia microformat page includes the Vcard example.
ALamb: there has been a question about whether/what format it should take, whether metadata or microformat, or jfun format. What is proposed here is how the standard is applied than changing the standard itself.
TBartley: The scheme (everything from 4.5 onwards) and the mechanism for how that is explored and displayed. The YAML is designed to be read and written by a human. However this makes it easy to produce a poorly formatted YAML file (hence the template.) a more rigorous way would be to use XML and so this is trying to do this. If we asked for YAML files and they aren't savvy enough to do so, it will be poor.

ALamb: rather than defining the fields as rec. req, permissive, you rearranged according to what kind of data was being collected, and made reccomended, required etc as a field requirement. 
NO OBJECTIONS
ALamb: I assume it will be clear which are required and which are recommended. 

ALamb: With 4.10 TBartley suggested extending the manifest schema, having consulted software colleagues.
TBartley: I was trying to say that the scheme may be extended, but here are risks that is is extended in different ways. Therefore how do we maintain discoverability. E.g date updated is a specific field, whereas the author can add other fields e.g multiple emails to add more data to existing rather than re-creating it. 
ALamb: We are thinking about this being extended in the future. We should think about whether it will impact portability and distributed know-how.


### Review
ALamb: Review to be done in two weeks, with commenting available through the platform (by the 23rd August)
To those on the call: in terms of the human process of doing this, would you like some remote sessions where we discuss reviews together? Some dedicated facilitated time slots, or would you rather work independently?
RBowman: I'd struggle to block off a specific time.
Andres: Blocking time would work for me personally. 
Kshitiz: I would like blocked out time.
ALamb: Tom and I will propose three sessions over the next two weeks where we can work together to go through the standard. This is less a meeting and more a diary reminder. This gives the space for people to do this through the platform. 


### Next Steps
ALamb: the facilitators for the Warsaw session encouraged us to agree the field of the OKH documentation, but also to take it further and agree a programme or series of activities for the adoption of the standard between now and Dec 2020. There were 5 areas that you can see on the screen we called 'key results' that support the overall objective:
100 users that have at least one project that is 80% completed. (not just manifests with required fields but entities that have created more fully-filled out manifests.) In support of this we have a number of key results and we are trying to organise working groups around. The most urgent is publisihing a usable standard by 31st AUg.

1. Publish a useable stansard (led by Andrew, supported by Tom from SRepo and Martin from OSE) 
2. 500 things defined by this spec on different platforms (led by Andres)
3. Fundraising target (Neil, Emilio, Anna, Richard, Andrew.)
4. Implement and maintain a system to extend the specification long term (Martin, Tom)

We should have separate calls for each of the working groups, and the drivers for those working groups agree something here. The suggestion from Martin is that the agenda is: the current status; the needs and wishes; with coordination between the groups. With other options we need to be aware of to promote this more broadly. This is the sort of activity the working groups will be doing. Does anybody have any reactions to the idea of having working groups? Particularly to the drivers and contributors - is the SRepo platform the right platform for this communication?
ALamb: I propose that we use a slack channel for the working groups, would that work?
Emilio: I think it is useful to have personal contact. It could be a whatsapp group, or Slack, I would recommend slack, especially if we want people from other organisations. 
ALamb: Anything against this as an idea?
Kaspar: Slack isn't the ideal for a public group. I have other suggestions but am not sure what specifically. Slack can be difficult with others joining in. 
Richard: As regards funding, a public channel might not be helpful, as we might have to think more carefully about what is being said, which could hamper progress. 
ALamb: Tha Makernet Alliance has an email system so we could as email lists. That is for the drivers of these different areas to consider. I suggest meeting of WGs every six weeks would be helpful. A review getting all working groups together every couple of months would be useful, with continuing to document public information through the SRepo platform. Does anyone have objections?

TBartley: with different targets, we could have a public dashboard of measureables, and let people work how they wish. 
Andres: contributors could meet regularly.
ALamb: After 23 August we will split into WGroups and calls will be focussed on these, on a monthly basis. 

TBartley: For Medium-term, the groups on this call is all OKH is. It is up to you to take ownership of this. SRepo will facilitate, but the ownership belongs to the group. 

ALAmb: If there are other people we would like to bring into the OKH group, particularly people who might want to have a look, with all the documentation SRepo have been putting up and to help us roll out and encourage the adoption of the standard please do so. Particuarly to myself and steering groups. For example I was at the Fab Lab groups in Finland who have been developing documentation tools and a mobile app to help makers document as they go. Also a Wikifactory global hackathon, and the idea of a docubot that had a camera in it which had a button and camera to record steps. These people may be interested in joining the OKH process as we move into the adoption stages. Please expand this community and get them involved.

### AOB 
Kaspar: can you share information about the abve two projects? 
ALamb: I will include links to the above projects into the chat. I shared Yani from Finand's details with RBowman in the group chat.




