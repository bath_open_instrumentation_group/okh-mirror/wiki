# Attendees 
1. Tom Bartley
2. Emily Cook 
3. Andrew Lamb 
4. Jeremy Bonsovin
5. Martin Hauer 
6. Andres joya  
7. Richard Bowman 
8. Kshitiz Khanal

# Apologies
Julian Stirling, Andre Maia Chagas, Tobey Wenzel, John Schull, Diderick Van Wingerden, Anna Lowe, Michael Weinberg, Brynmoor John, Kyle Wiens, Cameron Burgess, Brian Garret, Emilio Velis, Kaspar Emanuel

# Agenda
1. Publishing V1
* Brief overview (TB)
* Deferred Issues

2. Warsaw next steps
* Implementing a system to maintain and extend spec (Martin)
* Raising funding (Andrew)
* A searchable index of 100+ projects (Martin)
* Securing users (people/orgs/platforms)  
* 500 OKH Manifests (Andre)
* "conquer our street" strategy (Andre)
* Potential to publicise in Journal of Open Hardware through Jeremy
* Sharing on social media (using Jeremy’s board)

3. AOB

4. Next working group meeting date


# Notes
TB ran through broadly what has been happening in improvements to the standard in the past couple of weeks. 
The group were asked to share outstanding worries, concerns or questions about the standard thus far. There were no concerns or questions raised. 
EC ran through the project plan she had been working on, outlining where to find the mailing list, the basic plans and then invited the group to contribute other suggestions and updates to contribute further. (please see here.)

### Working Group 3 
AL clarified the intentions for OKH. We would have a landing page for openknowhow.org.  OKH is for the broad concept with the project aiming to open up know-how more generally, not just about documentation.  We would therefore have a standard for machine mapping.  capability mapping, components and materials standard, people and skill standard etc. There would be a web page which signposts people to different standards, not just this documentation but a series of standards or tools that would enable open know-how. 
AL asked the group if they would be happy that the broader remit includes these components. 
MH It sounds great, is there anything which already exists in industry to categorise and map knowledge and skills?
AL the next process I will be working on through Shuttleworth funding will be mapping of machines. There was some interest in Warsaw that we could do something already mapping electronics components material standards.
 AL Would the group be happy that the site links a variety of OKH standards.
EV mentioned that we could promote our work to IETF (Internet engineering task force, OSHWA website, GOSHH forum and promoting to these forums. 
EV also added that it is a good idea to have more components to OKH, but we should somehow engage with other organisations who are making standards for the web, to see who is already working on what, and understand their ideas on technical feasibility and internet best practice. 
AL Messaging around why we are doing what we are doing and ‘direction of travel’ should be well documented to move towards an internet of manufacturing. 
MH - maybe the Open Hardware Fund which provides financial funding for open source hardware projects, who are looking to gather funds to give out to support research in this area. This would be connected to their work, so we may get in contact with them to discuss with their networks and stakeholders.

### Working Group 1
AL asked the group if they required anything else to accompany the launching of the spec. (none suggested)


### Working Group 2
RB had applied for an internal academic fund at Bath uni which wasn’t successful. AL asked whether we should be following similar funds? RB thinks that it would be a stretch to fund this from universities as money is mostly for research, and this is not research. Pitched this particular bid as maximising impact from research. 
AL for small amounts of money indigo trust, Lloyds register foundation could work. Royal academy of engineering received money from Lloyds around safety, and there is scope for us to pitch our activities from safety perspective. This group will identify what donors we should be looking for, and some governance agreements around this. 
EC clarified who would be writing these bids? Would this be ALs role?
AL this is something that depends on the opportunity, we would all be doing this using one pagers, during our existing conversations with funders. A small group of people should coordinate these. When it comes to writing, we could employ a grant writer, or some other people have been using OKH already in other bids. Emilio could you tell us a little more about this….
EV I have been writing OKH into a number of bids around business education etc
EV I have been looking for funding opportunities including OKH. Would we do this as different organisations? Or would we form a consortia? I saw in Canada that they have some interesting funds, specifically for implementing in education. I could have made this work, but they needed a consortium of organisations, and I wasn’t sure which of our organisations would formally belong to this consortium. 
AL we could use the working group list to publicise bids and organisations could opt in, in an ad-hoc manner. Provided we are making this transparent, and the governance system is in place for how the money will be spent and by whom, this should work. 

### Working Group 4
MH this work is based on open-source ecology. In Warsaw I presented ‘over the wiki’ which looks at crawlers. We would build a crawler that searches manifest files for information, we would be able to filter for projects with these files.. We are making progress here. For example OHO just got launched a week ago. The problem right now with this is that the search results are do it yourself projects. The manifest files come in at the right time. We need a developer to keep up to date with the crawler technology, or funding as the company that was contributing and developing the crawlers have been doing this in their free time. Now with the beta launch, they need to prioritise funded projects. 
AL how much of a developers time would be needed?
MH most of the developers are based in South America so it is not too expensive
AL would a funding bid of a minimum of 5,00 euros work? This would open up Indigo trust who do small grants.
MH yes that would help. We can also look for funding on our side which might make it possible. There is not much more needed to complete this working group, we would just need to think about where the manifest files would be published.
AL We will also need to look into a version 1 of a portability standard in our funding. Does anyone else have any other comments?
TB having a counter on the manifest as well as displaying it on the OHO website would be useful.
EV Exploring an MOU with OHO to work together and have the content crawled. This could be a good entry point to invite organisations, we can have an index on the page but also create manifest files if this works well.

### Working Group 5. 
AL we need an idea of governance and how we coordinate funding to make sure all working groups dont clash in their asks. This piece of work is about extending V1 that has been agreed in this call. After the maintenance of the standard is done, TB could you explain how you think this works at SRepo at the moment?
TB Martin has made a suggestion in a proposal about how we would take the V1 into portability. This will need an editorial team who will manage this process, take ownership of changes that come through. When a person has an idea for a change, they will make the edits directly on the proposal branch, the editorial team will be notified and have a chance to discuss and implement these changes. 
MH we are having a similar problem with the DIN standard. I am writing a contributions guide that should handle this problem. It could be adjusted for these needs. The proposal on SRepo goes deeply into decision making. As soon as my contribution guide is available for public comment I’ll make it available. IF anyone would like to join this I’ll make this available and we can exchange contents. 
AL part of the governance will be around the people leading the working groups to connect and collaborate regularly. 
EC we are currently using mailchimp and the makernet account, I have created sub-groups. Ideally a mail would go out and then this would continue in a reply-all basis, but it could be that all groups have their own ways of working. 
AL I have used google groups before, people can reply as and when they need. I propose that this is not decided now, but at a later date. 
EC what would be the next call date?
AL all calls should be open and anyone can dial in to any call. I propose the 18th October for the next update. 

# Actions
ALL - populate their social media (Personal/Organisational) here, in order to share successes and news regarding OKH. 
ALL - book 3pm UK time on the 18th October for the next General Working group meeting. 
ALL - to arrange a working group meeting in the interim to progress activity amidst each group.
EC - draft a press release
EC - develop one-pagers for the group to use



