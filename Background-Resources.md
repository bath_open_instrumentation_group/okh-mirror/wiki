List of background resources used in developing this standard.

# Schema, etc provided by stakeholders

* [DocuBricks XML Format](https://www.docubricks.com/software.jsp)
* [FieldReady Management information and documentation templates](https://drive.google.com/drive/folders/1RMBvrklw_llef5KlQ3253Jknrb_3rXrT)


# Interfacing Standards
## Open Source Hardware Definition
The [open-source hardware statement of principles and definition](https://www.oshwa.org/definition/) were developed by members of the OSHWA board and working group along with others. 

### OSH Self-certification

[OSHWA Certification](https://certification.oshwa.org/) provides an easy and straightforward way for producers to indicate that their products meet a uniform and well-defined standard for open-source compliance.

## DIN SPEC 3105-1 Open Source Hardware – Requirements for Technical Documentation

A DIN specification under development. Describes the documentation required for a Piece of Hardware to be classified as Open Source Hardware and supports associated certification schemes. 

## Potential complementary standards

### Schema.org
A collaborative, community activity with a mission to create, maintain, and promote schemas for structured data on the Internet, on web pages, in email messages, and beyond.

[Schema.org](https://schema.org) is defined as two hierarchies: one for textual property values, and one for the things that they describe. Applicable analogues for OSH are [SoftwareSoureCode](https://schema.org/SoftwareSourceCode) and [Product](https://schema.org/Product).

# Guide to open standards for data
[This guidebook](http://standards.theodi.org/) from the Open Data Institute helps people and organisations create, develop and adopt open standards for data. It supports a variety of users, including policy leads, domain experts and technologists.