## View the candidate

The candidate is based on the agreement of the Open Know-How working group members at a workshop held in Warsaw in July 2019. The primary outputs from the working session can be accessed [here](https://drive.google.com/open?id=1VoKKQiMEPzQqe3rATXNyGPONXIHxtgKF).

## Commenting on the candidate
Commenting is visible publicly through the [proposals screen](../issues), but you will need to log in to add comments.

If you are a member of the Open Know-How Working Group, please log in using the credentials provided to you in July. If you are not a member of the Working Group and would like to comment, please contact [hello@standardsrepo.com](mailto:hello@standardsrepo.com) and we will provide you with credentials.

Please review the content available on the [Master branch](../src/branch/master/1). This is the candidate.

To propose a change to the candidate please create a [Proposal](../issues). 

Prior to creating a new Proposal, please review the existing open proposals to see if your comment has been made by someone else. You can discuss the proposal with others on the proposal conversation.

When creating a new Proposal:
1. Provide a clear name to summarise the changes you wish to be made
2. Provide a description to explain how the changes would be introduced to the specification and the benefits the change would have.

A member of the StandardsRepo team will review the Proposals periodically before August 31st and revise the draft accordingly. If you would like to edit the draft directly please contact [Tom Bartley](mailto:tom@standardsrepo.com) to request authoring rights for the repo.

Any proposed changes to the draft will be shared with the wider working group prior to being accepted into the master version.