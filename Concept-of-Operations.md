Options for the concept of operations enabled by this standard will appear here.

The draw.io file can be accessed [here](https://drive.google.com/file/d/1FM7uatjNgmwaMdWQUw-GzJQTq0rRUsKo) 



# A brief overview of the project:
## What is Open Know how?
Open Know-How is all about sharing how to make things. With dozens of open hardware organisations and over 80 content hosting platforms and thousands of designers sharing open hardware designs there is little consistency as to how know-how is documented or shared. 

## What problem/s are we trying to solve?
The Open Know-How Working Group is developing an open standard to make it easier to share and find documentation on how to make things. We believe this will be of benefit to:
* designers (who want to see their products used more widely and to be able to focus their design effort on improving what already exists instead of replicating it.)
* users (who then have a better chance of finding a design that meets their particular need),  
* platforms (who will benefit from an increase in the number of people engaged in developing designs because the barriers to entry will be lower). 

## What will the working group do?
The Working Group is developing a draft on the StandardsRepo open standardisation platform and using the wiki and proposals to document decision making. A draft version of the standard will be circulated for comment by the end of June 2019 with the aim of a version 1 to be published by the end of the summer. By bringing people together we hope to get agreement that the standard will be adopted and volunteers to pilot it prior to publication. Development of the standard and all decision making including notes from the event will be published openly and we welcome participation from interested parties across the open hardware community.

## How to get involved:
* Confirm your involvement by attending OKHWG meetings and therefore being added to the mailing list. (email emily.cook@standardsrepo.com)
* [Fill out the Google Survey](https://docs.google.com/forms/d/e/1FAIpQLScQmzIQtSbKDffDSIqxz4jkQrL3kRSVpWsulBAptAIapXP3Sg/viewform?usp=sf_link) to share your knowledge with us about what is already out there
* [Arrange a call with Standards Repo](https://calendly.com/emily-sr/okhwg) to share your knowledge and understanding to help scope what is needed in an OKH standard.



