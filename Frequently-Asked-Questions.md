# Who is the Open Know-How Working Group?

# How do I participate in the development of the Open Know-How specification?
The Open Know-How specification is developed by the Open Know-How Working Group. Please contact us via the [MakerNet Alliance](https://makernetalliance.org) to get involved. 
# How is the Open Know-How specification governed?
Governance for the Open Know-How specification will be agreed during a working group meeting in July.