**Andrew Lamb**
Works at Fieldready. Has secured a Shuttleworth Foundation grant to support the creation of an OKH standard to help improve the searchability of OKH Hardware.

**Tom Bartley**
Founder of Standards Repo - commissioned by Andrew Lamb. SRepo was founded in late 2018.  We believe that a community-driven approach is how we get people involved. We believe in open, collaboration and achieving consensus but are not in the open hardware world. We have been brought in to help to develop the standard as external practitioners who have expertise in writing standards but are impartial in the Open Source Hardware world. We are a software company who have a git-based platform with version control. We have been doing background reading about your work and how you document hardware.  We will be reaching out to reach out to people about what people are working on and what is happening and capture this.. The SRepo platform will host most of the conversations for this working group.

**Emily Cook**
Colleague of Tom at Standards Repo. Has a background in programme management and will be providing the secretariat capabilities for the OKHWG. 

**Nathan Parker**
Founder of makernet (makernet.work) which is a platform for tying together the maker movement ad creating the data backbone to integrate the maker movement with manufacturing and workforce development and disaster response. I am one of the main organisers of the makernet alliance OKH internet of manufacturing summit coming up in Warsaw.

**Jon Shull**
Founder of ‘enable’ global network of volunteers using 3D printers to make open-source prosthetics. Documentation is not their strong suit, so this is a really interesting project. 

**Naiomi Lundman**
Hi all, may have missed giving an intro. Am Naiomi Lundman with Humanitarian Makers. Have worked with some of you in the past. We aim to leverage the power of the network to make quality items where there is acute need. My background is using business methods and tools to create value for stakeholders (employees, community, etc). Believe open hardware will play a role in opening markets and /or availing solutions in areas facing acute needs. 

**Kaspar Emanuel**
Works for Kitspace.org which is for sharing open source hardware and electronics. Also recently started working with Julian Stuirling at Bath University on the open flexure project which is a 3D printable microscope. We are working on documentation tools for this also.

**Jeremy Bonvoisin**
Works in the field of Teaching and Research at Bath university I have recently started different research initiatives in open source hardware in recent years from colleagues in Berlin we have been granted an EU-funded research project with 19 companies and research institutes across Europe. Including wikifactory. I am also part of the standardisation process in Open Source Hardware in Germany. The specification for open Source hardware, includes. They are a big name of the field in writing standards on writing documentation, and as a result have set up the journal of open hardware.

**Kshitiz Khanal**
Did a chapter of open knowledge in Nepal. background of Mechanical Engineering, designing and building appropriate technologies such as biomass gassifiers and in doing so I refer to lots of open hardware documentations and wite some of the documents myself.

**Alex Petsiuk** (on behalf of Joshua Pearce, MIT) 
From Michigan Tech with a background in Electrical Engineering and Robotics. I work with Josua Gills on making 3D printers smarter. 

**Julian Stirling**
Univerity of Bath working with Richard Bowman. The team has an open source microscope and we are looking for software tools for documenting it. We are currently working in markdown but we re lookimg at other way to build materials automatically. 

**Jean-Michel Molenaar**
Project manager at Terre Des Hommes (NGO) use Fablabs in development and humanitsarian situations (we have labs in Greece, Gaza, Ukraine, Burkina Faso, Romania, Hungary and Djibuti with IOM.) These spaces use as much open hardware and software as can be convinced!

**Diderik van Wingerden**
I am an independent consultant and expert on digital social innovation. Last year I was on the Expert Panel of Danish Design Center's REMODEL program, now a co-creator of the DIN SPEC for Open Source Hardware and part of the Entrepreneurship working group. Also working for Dance4Life, Light for the World, Liliane Foundation and Health[e]Foundation on social innovation, advocating open source whenever possible.

**Richard Hulskes**
co founder of Wevolver.com and Welder.app . Wevolver enables engineers to discover and learn about engineering technologies. Welder is a public BETA gitbased version control system for hardware projects. Welder is currently used by a consortium Careables.org for open healthcare projects. Next month Mozilla starts using it for their new IoT projects.

**Raya Sharbain**
Raya Sharbain from Jordan. Raya is Anna Lowe's colleague setting up makerspaces in Azraq ton and Azraq Refugee Camp. I also volunteer with the Jordan Open Source Association where, and we're interested in open source hardware.

**Jenny Molloy**
I work on open source hardware and biological materials for local biomanufacturing, based at the University of Cambridge. I am interested to learn more as biology has a lot of "know how" that my project needs to share effectively!

**Toby Wenzel**
co-founder of Docubricks initiative and developing an open source standard for Hardware documentations, also an online repository. Also co-founder of the Journal of Open-Source Hardware (the only journal reviewing documentations and ensuring they are of good quality.) Otherwise I am a scientist in European Lab for molecular biology.
