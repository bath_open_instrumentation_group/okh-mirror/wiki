# Welcome to the Open Know-How Specification.
View V1 of the standard by clicking [this link] (https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/src/branch/master/1) or clicking on the tab above entitles 'standard.'

## Using this standard as a project creator

1. Quickly go through [the standard](https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/src/branch/master/1) or at least look at the template in section 4.12
2. Write an `okh.yml` file for your project
3. Check that it's ok using [the validator](https://buildinstructions.org/okh-validator/)
4. Add a link to your manifest to [this CSV](https://github.com/amchagas/okh-log/blob/master/projects_okhs.csv)

## Background
The standard was initiated by the Open Know-How Working Group, part of the MakerNet Alliance. 

The group prepared a [concept document](https://docs.google.com/document/d/1efwYNbrQ6mt4J9s7FYKYZ28l_A-IurdFCwA7TptbqSM/edit) to articulate the reasoning and needs behind the standard. 

## Introduction
Open Know-How is all about sharing how to make things. With dozens of open hardware organisations and over 80 content-hosting platforms and thousands of designers sharing open hardware designs there is little consistency as to how know-how is documented or shared. Makers struggle to locate what they need, not knowing which platform to find it, what the intended use is and therefore struggle to adapt designs. Whilst the knowledge is 'open' is not freely flowing in the spirit that open-ness suggests. 
## Objectives of the Standard
We are particularly interested in the accessibility of knowledge relating to making things useful in humanitarian and development situations, but we believe firmly that the more universal the standard is the more useful it will be. Our principle is that the standard should be open, whether or not the designs it is used to document are released openly.
 
The scope of what ‘things’ would be covered by this standard needs to be defined, but the current thinking is that it would include simple things made by both digital and analogue manufacturing techniques, through to more complex projects involving electronics and/or assembly of multiple types of materials. The standard would encompass information on checking quality as well as making the objects.

Open Know-How will make it easier to share and find documentation on how to make things and will reduce crossover/duplication of effort across sectors.

## Scope of Investigation
The strategy for this piece of work is to carry out an initial community listening/feedback exercise where we gather some key information about the issues and successes which currently exist in publishing and accessing open source hardware. This will then indicate where we should focus to produce an initial draft of the strategy for review at the OKH event in Warsaw in early July, followed by version 1 to be published by the end of summer 2019. 

Following our initial consultation with members of the OKH community, we have developed an understanding of the scope of the task at hand, and an idea of what is the biggest priority to focus on to ensure a useable and effective standard.

## Info page contents

1. [Work programme](https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/wiki/Work-Programme) 
2. [Scoping](https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/wiki/Scoping)
3. [Participants](https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/wiki/Participants)
4. [Background documents](https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/wiki/Background-Resources)
5. [Commenting on the candidate] (https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/wiki/Commenting-on-the-candidate)
6. [Software](https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/wiki/Software)
7. [Minutes and notes](https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/wiki/Minutes-and-notes)
8. [Frequently Asked Questions](https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/wiki/Frequently-Asked-Questions)
9. [License and intellectual property rights](https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/wiki/License-and-Intellectual-Property-Rights)