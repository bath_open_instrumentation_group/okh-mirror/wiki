The Open Know-How specification is licensed under the [Creative Commons Attribution 4.0 International License](http://https://creativecommons.org/licenses/by/4.0/). 

It is a condition of participation that contributors also make their contributions under the same license and confirm that they are not contributin any intellectual property for which they do not have right to do so. 

Contribution includes:
* Raising issues / proposals
* Reviewing content
* Contributing proposed text to the draft
* Participating in meetings, workshops and calls, etc.