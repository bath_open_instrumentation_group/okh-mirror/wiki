**Attendee:** Robert Mies, Martin Häuer, Pierre-Alexis Ciavaldini, Kaspar Emanuel, Cecilia Ho, Emilio Velis, Andres Barreiro (Wikifactory).

**Apologies:** Jeremy Bonvoisin.

**Brief Notes on the YAML meeting:**
The detailed debate and decision was made in the [proposal](https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/issues/269). We repeated this discussion from different perspectives and came to the decision to 
1. migrate to TOML (as discussed in the proposal).
2. define requirements for the file format and update them as we still don't know all use cases of the manifest file. E.g. if someone wants to include manifest files in a graph database, JSON or RDF may be a better choice.


**Actions:**
1. @Kaspar to translation (+minor bugfixes +deleting microformat section +closing corresponding issues → thus releasing a new version of the standard) the standard from YAML to TOML [now]
2. @Kaspar to have a meeting with Tom StandardsRepo to discuss details of editing and approval permission [now].
  * Permission levels on StandardsRepo: Everyone in the group update the label for level one (for the whole group). 
  * Permission levels for people to close proposal, and make the sharing changes.  
3. @Tom StandardsRepo to share tutorials how to use the platform (manage labels for proposals, manage branches, link to other platforms (e.g. kaspar's tool to check manifest files)) [now]
4. @Martin to create a workflow to process proposals (e.g. via labels) [now]
5. @Martin to migrate Update Policy from gitlab to StandardsRepo [now]
6. @Kaspar will do the branch management of the standard (don't remember him saying that, but Martin has this in the notes).
7. @Joel (part of Kaspar's research group) to create template for manifests: A webform rather than a template. A Javascript to be held on a Gitlab page [after TOML translation].

**Decisions:**
* Use [a separate tab on master plan](https://docs.google.com/spreadsheets/d/1C-ZjpK2JkE3fJ5W1OO7w_z4mWcErM3ay74CCCkjc8Fc/edit#gid=1422257122) for task managment +  editing rights for everyone for this tab) 
* Use [proposal for a manifest migrator](https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/issues/277) in order to convert manifests from older standard versions to newer versions (e.g. to solve conflicts regarding not-anymore-existent data fields, new mandatory data fields, change of data format in fields (Boolean→Integer), change in file formats (YAML→TOML))
* Use the [proposal for creating an implementation](https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/issues/281) aka testing environment for the standard done. 
* **#UpdatePolicy stakeholder categories** defined (updated on Master Plan).
  1. OSH Developer (can be pulled in via manifest creation).
  2. Maker using OSH documentation.
  3. OSH upload platform (kitspace.org, makernet.org, wikifactory).
  4. OSH search engine / platforms crawling manifest files (oho.wiki).

**Discussions:**
Validator: Prototype for discoverability (manifests)https://buildinstructions.org/okh-validator/.

**Lesson logs:**
From the lesson of the YAML issue, before we publish the next version or any new standard, we need validators to test it first. For the team to receive feedback and make changes throughout the process. 

**Next meeting:** 21 Nov 2019 https://meet.jit.si/OKHMaintaintheStandard  