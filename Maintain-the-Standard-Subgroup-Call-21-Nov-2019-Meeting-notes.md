**Attendees**:Kaspar, Robert, Emilio, Martin (minutes)
Martin Häuer (minutes), Robert Mies, Kaspar	Emanuel, Emilio	Velis
**Apologies**:Jérémy Bonvoisin, Pierre-Alexis Ciavaldini

**Updates**:
1. A workflow for proposals and shared changes has been created and approved (consent), find the details [here](https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/issues/322) 
  * Martin will make a selection of proposals and shared changes to decide about them in the next meeting (general call & maintenance call)
2. A version number convention has been created and approved (consent); find the details [here](https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/issues/323)
3. Wikifactory will be informed about the YAML decision from our last general call (which they couldn't attend) (done; via YAML proposal)
4. After finding a consent with wikifactory, @Kaspar will finish the new version of the standard (see dev branch; depending on the consent, this will be 1.1.0 or 1.0.1)
5. Important 'maintenance tools' we agreed on are
  * validator (checks compliance of a manifest file) done, please find it [here](https://buildinstructions.org/okh-validator/); feature request: adjust this script for git repos so manifest files are automatically checked
  * converter (release-specific script to convert manifest files to higher major versions of the standard) - a crucial To Do for the next major release; so not an issue yet
  *	generator (facilitates manifest file creation) work has been started by a college of Kaspar (Joe); Kaspar sends Martin a reference to the current state; Martin will look for coders to contribute there

  **Actions**: 
1. Martin to prepare a selection of merge requests (shared changes) to decide on by 2 Jan 2020 
2. Kaspar to finish version 1.0.1 / 1.1.0 in the dev branch	by 2 Jan 2020 
3. About generator: Kaspar to send Martin a reference to the current state (about the work Joe has worked on) 
4. About generator: Martin to find coders to contribute  

  **Next meeting**: 
  2 Jan 2020 at https://meet.jit.si/OKHMaintaintheStandard 