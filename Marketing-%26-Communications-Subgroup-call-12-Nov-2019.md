**Attendees:** André, Kaspar, Julian, Robert, Jérémy, Andrew.
Agreement that recording is not necessary. 
  
**Concerns about creating YAML manifests since we decided to TOML now. We need an agreement that the file format won’t change in the future and we need an agreement that we can go with TOML now. Without this agreement, the marketing group cannot proceed further.**

**Action**: go and consult platforms to get a thumb up (“market research”):
* Careables (Andrew reaches out to Enrico)
* Wevolver (Andrew reaches out to Bram)
* Wikifactory (they want TOML)
* Fieldready (ok with YAML and TOML)
* Kitspace (ok with YAML and TOML)
* Appropedia (Jeremy)
* Thingiverse (Andrew) 
* Prusa (Andre)
* Hackaday (Kaspar)
* Open Behaviour (Andre)
* PLOS Open Source Toolkit (Andre)
* Instructables (Andrew)
* Wikifab (?)
* OHWR CERN (Kaspar)

**Objective for end of the year (Christmas):** have a clear situation on TOML, i.e. agreement from major players and stable “beta” version of the standard (not in the remit of marketing subgroup). 

**GOSH:**
* Present the idea (make it easy to find hardware)
* Tell the story, where we started, the whole initiative
* Invitation to get involved
* Subscribe to the https://openknowhow.org/ mailchimp list
* Send links to their projects and some contact data so we can ping them back

**Website feedback:**
Twitter link takes to “following” page and not to main twitter account

