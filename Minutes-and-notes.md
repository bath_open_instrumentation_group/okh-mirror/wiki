* 2019/02/21 Open Know-How Working Group - Initial Call
    * [Notes](https://docs.google.com/document/d/1I-nYIk4fyjOFKCcnw7Wq-TBqFSwhjHkULZswdwmne54/edit)
    * [Recording](https://www.dropbox.com/s/jngof87nomiwpo8/makernetalliance%20on%202019-02-21%2017%3A06.mp4?dl=0)
* 2019/06/12 Open Know-How Working Group - Standard kick-off Call
    * [Invitation Email](https://mailchi.mp/2d215f884750/open-know-how-working-group-call)
    * [Notes](https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/wiki/2019%2F06%2F12-Minutes--OKH-Working-Group)
    * [Recording](https://drive.google.com/open?id=1qf2oOP8dXJp01yqhzM2DaSc-7dFrxWu9)
    * [Follow up/next steps email] (https://mailchi.mp/95e72e137613/minutes-recording-and-next-steps-okh-working-group)
* 2019/07/01 Open Know-How Working Group - Call
    * [Invitation email] (https://mailchi.mp/[xxxxxx]/confirmed-okh-briefing-call-1st-july)
    * [Reminder email](http://https://mailchi.mp/abdde3e2b8c8/reminder-call-monday-open-know-how-scope-for-review) with links to progress thus far
    * [Notes] (https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/wiki/2019%2F07%2F01-Minutes-OKH-Working-Group)
    * [Recording] (https://recordings.join.me/6cR1D9QerU2eQDbAW5zPlw)
* 2019/08/08 Open Know-How Working Group - Call
    * [Notes] (https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/wiki/2019%2F08%2F08-Working-Group-Call-Notes)
    * [Recording] (https://drive.google.com/open?id=1gXfwhureY1qZLy4XsyLiyb4fiStStBdy)
    * [Presentation] (https://docs.google.com/presentation/d/1Ktlu2kEsz7PVy2Rc6lDzlJtPT0UUQMay8kO9QUieskg/edit#slide=id.p)
* 2019/09/13 Open Know How Sign-off call
    * [Notes] (https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/wiki/2019%2F09%2F13-OKH-WG-Meeting-Minutes)
    * [Recording] (https://recordings.join.me/4A5lzK7qoESrFfGsHsLxNg)
    * [Resources] (https://docs.google.com/spreadsheets/d/1C-ZjpK2JkE3fJ5W1OO7w_z4mWcErM3ay74CCCkjc8Fc/edit?usp=sharing)
* 2019/10/14 Subgroup Web Crawler Call 
    * [Notes] (https://docs.google.com/document/d/1gNrB13D_vWTZqjg69Sc5v4P4sPYQE58mj3pCL6PYbNE/edit?usp=sharing)
* 2019/10/10 Subgroup Marketing & Communications Call 
    * [Notes] (https://docs.google.com/document/d/19wnpltS-mhJIIHkyHXcPr9gjpwiALtAtwyMbZ-EHBxo/edit?usp=sharing)
    * [Recording] (https://drive.google.com/file/d/1Q4-qgTVQ0UecEHlyvszuTNy9Po2zZL2L/view?usp=sharing)
* 2019/10/15 Subgroup Fundraising Call 
    * [Notes] (https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/wiki/OKH-Fundraising-Meeting-Notes-15-Oct-2019)
* 2019/10/18 Open Know-How General Call 
    * [Agenda] (https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/wiki/OKH-General-Call-Agenda-18-Oct-2019)
    * [Notes] (https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/wiki/OKH-General-Call-Meeting-Notes-18-Oct-2019)
    * [Recording 1] (https://recordings.join.me/z50_dwwpmkK4z_xLHYnL4g)
    * [Recording 2] (https://recordings.join.me/JpnZz1s-hk6nL8nV3aNlpw)
    * [How-to make change to the standard] (https://vimeo.com/375701863)
* 2019/10/24 Subgroup Maintain the Standard Call
    * [Notes] (https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/wiki/Maintain-the-Standard-Subgroup-Call-%28included-YAML-issue-call%29-24-Oct-2019-Meeting-notes-) 
* 2019/11/12 Subgroup Marketing & Communications Call
    * [Notes] (https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/wiki/Marketing-%26-Communications-Subgroup-call-12-Nov-2019)
* 2019/11/21 Subgroup Maintain the Standard Call
    * [Notes] (https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/wiki/Maintain-the-Standard-Subgroup-Call-21-Nov-2019-Meeting-notes)
* 2019/11/13 Working Group Call
    * [Notes] (https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/wiki/OKH-General-Call-Nov-13-2019-)
    * [Recording] (https://recordings.join.me/o5bycNpDEEaV3vFtDh3FTA)
* 2020/01/07 Working Group Call
    * [Notes](https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/wiki/OKH-Updates-07-January-2020) 
    * [Recording] (https://recordings.join.me/XH1R5dICnkmC3Q8TVrI51w) 