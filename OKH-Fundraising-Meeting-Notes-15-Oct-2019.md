**Attendee:** Andrew Lamb, Emilio Velis, John Gershenson, Richard Bowman, Cecilia Ho

**Apologies:** Neil Noble

**Actions:**
* Andrew & Cecilia to create a fundraising prospect list - completed - in the [Open Know-How Master Plan](https://docs.google.com/spreadsheets/d/1C-ZjpK2JkE3fJ5W1OO7w_z4mWcErM3ay74CCCkjc8Fc/edit#gid=2139650764) 
* John and Andrew to have another meeting to talk about Intenet of Production fundings 
* Andrew to send follow up email to Open Tech Fund 
* Andrew to updates on fundraising opportunities after NetHope conference. Aid agency IT system people - once every two years.
* Andrew to update the master plan 
* For the whole group meeting: Ask everyone if they have objection in regards to collaboration with big corporates, Microsoft or Cisco putting any money into the group. ASK this on the group on Friday 

**Notes:**
*Cost proposals: $50,000 in total
$7,500 for Documentation: StandardsRepo retainer - On-demand support for any OKH project (stakeholder engagement service, technical drafting advice, procedural governance advice)  $7,500 a year. 
We have the current site on StandardsRepo for one year till 31 August 2020. After that, we can take it to other. 
 
*$40,000 for Portability
Andrew will be funding Open Know-How for machines himself with the Shuttleworth foundation, 37,000 USD.

*$2,000 for Welder to integrate the standard with their platform. It will take around 10 days to build the integration. 

**Fundraising goal:**
2.5 million USD from now and 2020 - $100,000 for OKH by July 2020

**Discussion points :** 
* Do we agree on how and where to spend on the fundings of $50,000. 
* What to spend on with the other $50,000. Maybe another conference in Bath, other working groups’ objectives (marketing and web crawler) another secretariat, for other platform to adopt the standard.
* Some ideas on spending:
* * Networking events 
* * One killer application  - Open Hardware Association about crawler
* * Guide, video or webinar showing people how to use the standard 
* Where do we get the money from
 
**Fundraising options: (Andrew)**
Foundation & Trust that interest in open initiatives 
Corporate sponsorship - not for Open type or community type of projects
Potential grant from government - standard body xx Foundation
From Royal Academy of Engineering 
Field Ready will be relocating some fundings to the mapping project for disaster areas and developing countries if Field Ready get the relevant fund raised.
** Fundraising opportunities: (Emilo):**
* $6,000 - [community prototype fund ](https://www.opentech.fund/funds/community-prototype-fund/)
* $100M - [Grant for the Web fund ](https://www.grantfortheweb.org/)to boost open, fair, and inclusive standards and innovation in web monetization. perfect fit. 
* Find a specific application about OKH - Emilio has been testing it with Appropedia. 
* OKH to develop an appropriate technology? 

Note: Andrew had a dinner with the Open Tech Fund - Chief Executive, send a follow up email!! 
 
**Academic fundraising opportunities (Richard)**
Tricky - Most of the academic funds for research
What about funding an open science event? - yes, it is possible, has to be relevant to research.
Digital medical imaging - not exactly relevant, except that Richard is trying to open hardware stage

**Internet of Production Alliance and Open Know-How (John)**
Better to aim for Larger foundation 
Application in research. Funding from within. - basic research
Infrastructure, document hardware, can be used to then make an impact.
National science foundation - enabling technology that opens up the world
 
**Discussion on collaborating with Microsoft and other corporate:**
IoP alliance will register a foundation very quickly. Open Linux System has funding from these big corporates as well. We want to think about a similar model of Open Linux System. We do need to have conversations more broadly in the future.
 
Support for fundraising: We could have bid writers.  

**Concerns:** 
Need to be careful on how to use the money. 
No matter where we get money from, we have to run it through with one of the organisations we run with OKH - if we run a foundation group, we can run as a charity, with tax benefits. Need to be careful about how to use the money, MSM and Field Ready might be able to help, but we need to be very careful, and open about it.
Wikipedia foundation might be able to help as well.

**Next meeting:**
13th November morning ET time.
 





