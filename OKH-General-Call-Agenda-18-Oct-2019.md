#### 1. Introductions - welcoming new joiners to the group (Andrew, All)
#### 
#### 2. Introduction to Open Know-How and the Documentation project (Andrew)
#### 
#### 3. Progress summary (Andrew)
#### 
#### 4. Share the[ Master Plan](https://docs.google.com/spreadsheets/d/1C-ZjpK2JkE3fJ5W1OO7w_z4mWcErM3ay74CCCkjc8Fc/edit#gid=1595259056) on Google Spreadsheet & discuss updating process (Andrew)
#### 
#### 5. Documentation working group: Standard
#### 
####    * Updates
####    * Tom: to demonstrate how to use StandardsRepo for making changes to the draft and answer any burning questions.
####    * Martin: Call to action for people from the working to join the editorial/steering group (this maintaining group)
####    * Beyond YMAL 
####  
#### 6. Documentation working group: Fundraising
#### 
####    * Updates
####    * Opportunities list
#### 
#### 7. Documentation working group: Marketing, Communications & Community  
#### 
####    * Updates  
####    * Presentation  
####    * Press Release
#### 
#### 8. Documentation working group: Web crawler
#### 
####    * Updates  
####    * OpenNEXT project interactions
#### 
#### 9. Next steps/Actions
#### 
#### 10. Updates on other Open Know-How projects e.g.: Machine mapping (Andrew)
#### 
#### 11. AOB
#### 
#### 12. Confirm next meeting 