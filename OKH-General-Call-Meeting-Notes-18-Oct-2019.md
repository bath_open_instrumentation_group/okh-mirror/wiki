**Attendee:** 
Michael Weinberg, Max (Mahmoud) Wardeh, Andrew Lamb, Martin Häuer, Tom Bartley, Richard Bowman, Andres Maia Chagas, Jérémy Bonvoisin, Julian Stirling, Pierre-Alexis, Andres Barreiro, Kaspar Emanuel, Emilio Velis, Cecilia Ho (14 ppl in total) 

For new member, here is the [introduction document](https://docs.google.com/presentation/d/1OGx06tNzhP8rWQxHh_-SO4cTnxczcSkRE-mVHdSSY0I/edit#slide=id.g60179f6f86_1_57) of Open Know-How.

**Call for actions:**
Feedback on the first version of [contribution guide policy ](https://gitlab.com/OSEGermany/open-source-standards---contribution-guide-resources/blob/master/Open%20Source%20Standards%20%E2%80%93%20Update%20Policy.md) drafted by the Maintain the Standard working group  

**Join the Maintain the Standard working group**
* We needs two or three more contributors for this group.  
* Responsibility: maintain the standard, permit to make changes, this is the group who can make decision on merging proposals and solutions for issues regarding the standard. Everyone can submit changes. 
* Current members: Martin Häuer, Jérémy Bonvoisin 
* Next meeting 24 Oct 17:30 PM on discussing the policy 
* [Link](https://join.skype.com/ldUq9rfcgT0t) to join the YAML meeting
* [Link](https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/issues/269#) to comment on the proposal (deadline 23 Oct)

**Actions:**
**Maintain the Standard working group**
* To submit the YAML issue proposal on StandardsRepo after the meeting on 24 Oct. 
* Decide on moving beyond the YMAL format, YMAL or not YMAL.

**Actions: Marketing & communication working group**
* Keep on communicating the standard. 
* Cecilia to make the website and upload the press release by 3 Nov 2019. 

**Actions: StandardsRepo**
* How-to video on how to make changes. 
* Share the meeting record video with Cecilia or upload to this meeting section.
* Priority issues on the platform. 

Actions: Secretariat
* Create new accounts and add collaborators to the platform 
* Coordinate with web designer for the openknowhow website 
* look at another platform for calendar invite and email communication 
 
**Decisions:**
* YAML issue: we keep on communicating the standard and the Maintain the Standard group will discuss about the YAML issue on 24 Oct. 
* Fundraising: there is no objection about collaboration with Microsoft & other big corporates, whose requirements will be considered seriously across the group. 
* Meeting: Monthly call, next meeting is on 13 Nov 4PM UK time.  

**Notes:**
V1 standard was agreed several weeks ago. 

**The YAML Discussion**
* YAML is an insecure technology. 
* Emilio: It could be a problem when implement it.
* Andres from Wikifactory: it's a big deal, we need to have more secured first, Wikifactory will not use the standard until this is sorted out. 
* Richard: The way we have used YMAL is safe, so regarding the security of YAML, if we are are using the subjects of YMAL that are safe, then there is no security problem. 
* Advice from Tom Bartley: create a proposal of the tools, keep it open and the Maintain the Standard group to decide on the tool to move from YAML.
* Do we need to review the YMAL before we communicate about the standard? 
* Decision: no. But this will need a longer conversation.

**How to update project and working group progress:**
Update the master plan or email Cecilia for her to update it 

**How-to in regards the StandardsRepo plaform**: 
1. Need an account -> Email Cecilia to create one 
2. How to prioritise the issues on the platform? Labele or flag the urgent issues 

