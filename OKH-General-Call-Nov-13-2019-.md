**Decisions:**
1.	We remain with YAML format, but will research changing the format in the future.
2.	Marketing working group will focus on those users who will hand-write their manifests as agreed in Warsaw (rather than also concentrating on platforms for initial uptake).
3.	Marketing working group will ask a number of design sharing platforms for their opinion on YAML or other formats.
4.	How we communicate about the current version of the standard: we are trialling the version 1, and we do expect it to change over time but using similar fields.

**Discussion points for the next call:**
1.	How do we decide what aspects or changes will be destabilised? Fundamental change once a year, but we might release an extension if there is no impact to others?
2.	YAML debate - we will look at it again in July 2020
3.	What field of information we are going to need for the standard?
 
**Marketing group updates:**

* •	Has gathered 16 manifests so far (listed here https://github.com/amchagas/okh-log/blob/master/projects_okhs.csv).
* •	Field Ready is writing up 113 manifests 
* •	Asking platforms if there is any objection to YAML format 

**Maintain the Standard group updates:**

* •	A detailed discussion was held regarding the YAML/TOML issue, and a number of proposals were considered. Decision was to remain with YAML. Notes from this discussion will be added to the existing discussions in the StandardsRepo platform.
* •	Kaspar will finish the new proposed version of the standard based on TOML rather than YAML for checking in the future with this group.
* •	Workflow for proposals and shared changes has been created and approved (consent); find the details here
* •	A version number convention has been created and approved (consent); find the details here
* •	Important 'maintenance tools' agreed on are:
* •	validator (checks compliance of a manifest file)
* •	done, see: https://buildinstructions.org/okh-validator/
* •	feature request: adjust this script for git repos so manifest files are automatically checked
* •	converter (release-specific script to convert manifest files to higher major versions of the standard)
* •	a crucial To Do for the next major release; so not an issue yet
* •	generator (facilitates manifest file creation) -  in progress 
 
**Fundraising group updates:**

* •	Andrew Lamb is progressing an application with the CISCO Foundation based on Field Ready’s previous work with them; it will be a larger application that has to include some impact in a developing country, but can be used to fund some of the costs of the Open Know-How work that have been proposed.
* •	Thanks to Andre Chagas for a connection made at MozFest, Andrew Lamb and Cecilia Ho held a meeting with the Sloan Foundation regarding a bid that will fund aspects of Open Know-How work as well as future standards around mapping.
* •	Other targets and possible expenditure items are included in the Google Sheet tool.

**About governance:** 
* •	Secretariat will send an update email after each subgroup meeting and general call to the whole OKH group with key takeouts; 
* •	Secretariat will send a clear agenda and updates of each working groups before the general call. 
* •	Secretariat will identify impactful decisions and actions from each working group meetings and activities and inform the related working groups. 
* •	All the debate and discussion should be recorded on the StandardsRepo platform. 

**Types of calls:** 
* 1.	working group calls for small decisions and informal consult; 
* 2.	general call for big decisions that need input from the wider team;
* 3.	emails are for informing the wider group with updates 

**Next meeting: 7 Feb 2020 4 PM GMT**
