Attendants: Theo Tay-Lodge, Julian Stirling, Emilio Velis, Kaspar Emanuel, Jérémy Bonvoisin, Rafaella Antoniou, Richard Bowman, Andre, Andres Maia Chagas Tobias Wenzel, Alessandra Schmidt, Pierre Alexis, Tom Bartley, Andrew Lamb, Cecilia Ho 

**Maintain the Standard Updates:**
- We now have a GitHub organisation. https://github.com/OpenKnowHow
- The org hosts the list of manifests (csv file) and source code for a search page: https://okh.now.sh
- Would be good to point a more official looking domain at it e.g. search.openknowhow.org, will need someone to set a CNAME record on the domain to alias.zeit.co
- Kasapr has added some instructions for using the standard which should help guide people to implement it:
https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/wiki/Home#using-this-standard-as-a-project-creator 
- If anyone would like to change something about the instructions but doesn't have wiki editing privileges, please let Kaspar know.
- "Sub" things: Kaspar and Tom has looked in the field "sub" which is not being very intuitive, Tom and Kaspar consider to subsititute for "uses".
- **Process of making & sharing changes:** Changes up to Maintain the Standards group and Bable (StandardRepo), but major and breaking changes need to be circulated and discussed in meetings.  
- Richard & Julian: I think we're fine with changes being made, so long as they are not major/"breaking changes" bug fixes needn't be circulated, but it would be good to circulate major changes as Kaspar says.
- Martin: developer from oho.wiki will include the CSV from GitHub in OHO's database and build a web form that helps creating the manifest until the end of the month. So that 
1) projects using the OKH-standard can be found and filtered on OHO
2) manifest files can be created in a survey-like environment.

**To submit manifests/URLS:**	https://github.com/amchagas/okh-log/blob/master/projects_okhs.csv You can put them in the GitHub or email Kasapar the link.

**Webcrawler updates**
Nothing new from the webcrawler meanwhile. Our resources are currently bond to DIN SPEC certification. In order to change this in the next weeks/months we'd need either a) a developer helping us or b) funding to pay one for it. We asked for this in one of our first general OKH calls. This would accelerate the whole process enormously. 

**Marketing Group updates**
- Need commitment from the group on the manifest - to communicate with platform on manifest 
- Work with 10 - 20 platforms 
- More hand code manifest before publishing 
- Generate more manifest to help reach ideas 

**Discussion on Manifest:**
- Hosting in GitHub, point the link to the field ready email then it is fine 
- Diversity in the manifest: different authors: Have at least two manifest from each members 
- Emilio: will have a demo table for manifest in New York open hardware summit 13 March 2020
- Pierre Alexis can create an online form to generate the format if that could be of any help, which could also feature a list of all manifests including the ones already listed on Github

**Actions:**
1. Hand write manifest!!! 
2. PA and Emilo to speak - Marketing 
3. Andre speak with PA on website - Marketing 
4. Jeremy to share the platform list - Marketing 
4. PA chat with Tom Bartley to discuss changes and see if StandardsRepo to include in retainer work that Shuttleworth is paying - Maintain the Standards 
6. Cecilia to work on Powerpoint - mid march for Corentin - Marketing 
7. Emilio to talk with team members on how to set this up on Appropedia - Maintain the Standards
8. Publish the funding application on the StandardRepo - Fundraising 
9. Alessandra and Emilio to catch up for OSHWA event - Fundraising 
10. PA to help manifest platform with Field Ready
11. Email list ?? 
12. Tom to catch up with Alessandra S - Open Know-Where

**Fundraising:**
- Netherland Net Foundation EU Next Generation Programme -Bid submitted 

**Upcoming event:**
- New York OSHWA summit 12 13 March- Andrew Lamb and Emilio will attend 
- Singapore Foss Asia https://fossasia.org/ 19 March - PA and Corentin will talk about Open Know-How 
 NY - have a demo table for the people in the working groups, probably a seminar with screen, second part of the demo, showcase the project to find meta data to engage people and how the manifest can be used. 
- Open Source Code Event in the UK - Julian will speak in the event in summer for his own project 
- Open electronics - biology conference, Tobias will be speaking a bit about documentation in his presentation, which Tobias will mention the manifest in his speech 

**Open Know-Where:**
- StandardsRepo changed name to Barble: motivation of the change of the name: name of barble, mix of barley and COO broader market , policy and procedure, standard the name kind of xx people make it neutral. 
- Mapping of hackspaces, manufacturers 
- Tom to catch up with Alessandra S

Next meeting: TBC 
