List of participants in the Open Know-How community.


# Strategic Partners
* Appropedia
* Careables
* Docubricks
* [e-NABLE](http://http://enablingthefuture.org/)
* Humanitarian Makers
* [MakerNet Alliance](https://makernetalliance.org)
* Open Source Ecology
* [Open Source Hardware Association](https://www.oshwa.org/)
* Wevolver
* WikiFactory

# Individual Contributors

# Funders
* [The Shuttleworth Foundation](https://www.shuttleworthfoundation.org/)

# Secretariat
* Secretariat, Cecilia Ho, cecilia.ho@fieldready.org 
* Secretariat, project management and technical authoring by [StandardsRepo](https://standardsrepo.com)