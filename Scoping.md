# Background
The standard was initiated by the Open Know-How Working Group, part of the MakerNet Alliance. 

The group prepared a [concept document](https://docs.google.com/document/d/1efwYNbrQ6mt4J9s7FYKYZ28l_A-IurdFCwA7TptbqSM/edit) to articulate the reasoning and needs behind the standard. 

# Introduction
Open Know-How is all about sharing how to make things. With dozens of open hardware organisations and over 80 content-hosting platforms and thousands of designers sharing open hardware designs there is little consistency as to how know-how is documented or shared. Makers struggle to locate what they need, not knowing which platform to find it, what the intended use is and therefore struggle to adapt designs. Whilst the knowledge is 'open' is not freely flowing in the spirit that open-ness suggests. 
# Objectives of the Standard
We are particularly interested in the accessibility of knowledge relating to making things useful in humanitarian and development situations, but we believe firmly that the more universal the standard is the more useful it will be. Our principle is that the standard should be open, whether or not the designs it is used to document are released openly.
 
The scope of what ‘things’ would be covered by this standard needs to be defined, but the current thinking is that it would include simple things made by both digital and analogue manufacturing techniques, through to more complex projects involving electronics and/or assembly of multiple types of materials. The standard would encompass information on checking quality as well as making the objects.

Open Know-How will make it easier to share and find documentation on how to make things and will reduce crossover/duplication of effort across sectors.

# Scope of Investigation
The strategy for this piece of work is to carry out an initial community listening/feedback exercise where we gather some key information about the issues and successes which currently exist in publishing and accessing open source hardware. This will then indicate where we should focus to produce an initial draft of the strategy for review at the OKH event in Warsaw in early July, followed by version 1 to be published by the end of summer 2019. 

Following our initial consultation with members of the OKH community, we have developed an understanding of the scope of the task at hand, and an idea of what is the biggest priority to focus on to ensure a useable and effective standard.

## Initial findings
Our conversations with community stakeholders suggest that there are a number of issues which currently hinder true open-ness of hardware. These have been considered and prioritised to give us a direction moving forward. What has become apparent from our conversations is that there are a number of broad elements which, if solved, will support a more open and accessible transfer of knowledge. We have prioritised these in order to focus resources effectively and produced 'levels of maturity' which indicate how the standard could evolve.



* Level 1: Discoverability - The most prevalent issue when speaking to stakeholders was the discoverability of designs. This will therefore be the initial focus of the standard.  
* Level 2: Portability -  The next area of focus in future iterations of the standard will focus on the portability of designs, ensuring that their integrity is not lost when the design is moved between platforms, or accessed by different makers. This could include addressing the issue of translatability, to ensure that makers can access the designs to solve their problems, no matter where they are in the world.    
* Level 3: Distributed knowledge systems -  The final phase would be ensuring a programme of changes to see what is practical, desirable, achievable for the community. 


## Next steps: Discoverability
The first stage will see us address the issue of discoverability.

The proposed solution is to create a manifest for designs which ensures that they are discoverable & in a consistent format. This information will be gleaned from some of the platforms which users are currently interacting with. We will be cross-referencing all of the different data listed n their manifests. Following this collection of data, we will be able to define these items into three categories of data (mandatory, recommended or permissive) to ensure that a design meets the criteria of 'discoverable.' 
You can follow the progress of this work [here.](https://docs.google.com/spreadsheets/d/1xuFjpqfih5JaNVMvxR6tsdWbswMiMbwNDmAT3s0UU2E/edit?usp=sharing)

# Target Operating Model

# Stakeholder discovery
## Sponsor 
Funds or otherwise facilitates the development or diffusion of open designs
 

## Problem owner
Has a problem to be solved by open hardware

*  "People who are looking for a design to solve a problem have no easy way to find out what already exists that could help." [1]
*  "Even where design files can be found, it can be difficult to tell how relevant that design is to the situation at hand, how thoroughly it has been tested, or how closely the product made resembles what was intended." [1]

## Designers
Creates designs they want others to make

* "Everyone who documents hardware has their own preferences" [1]
* "Want to see their products used more widely, and to be able to focus their design effort on improving what already exists instead of replicating it" [1]
* "have to adapt their data from a wide range of different platforms" [2] 
* There isn’t the consistency with open hardware that exists with open software.[2]
* Where to publish hardware? Which platform do you choose?  [2]


## Makers
Makes things from open designs

* Finding content is difficult when there is >80 platforms hosting open designs [2]

## Platforms
Lists, provides access to and/or supports the development of open designs

* "Each site has its own features and functions" [1]
* "Designs are spread across these sites and are not easily portable from one to another." [1]

## End users
Uses the hardware after it has been made

# Existing Specifications

Sponsor
* Careables

Designer
* FieldReady
* [Building FOSH following demand](https://github.com/FOSH-following-demand/equipment_repository_template)

Platform
* Wevolver
* Wikifactory
* [DocuBricks](https://www.docubricks.com/software.jsp)
* [Kitspace](https://kitspace.org/submit/)
* Instructables
* Thingiverse
* [Wikifab ](https://wikifab.org/wiki/Wikifab:Best_practices)
* [Welder]()
* [Go Commons](https://go-commons.gitlab.io/prometea/)

Standard
* DIN 3105 (draft rev 2)
* [OSH Definition](https://www.oshwa.org/definition/)
* [OSHWA Best Practices for Open-Source Hardware](https://www.oshwa.org/sharing-best-practices/)
* [Schema.org](http://https://schema.org)

[Comparison of existing specifications](https://docs.google.com/spreadsheets/d/1xuFjpqfih5JaNVMvxR6tsdWbswMiMbwNDmAT3s0UU2E/)

# Application
* Andre Maia Chagas - Concerned that making one general standard will be difficult as some documentation will not be possible globally (e.g. downloads - limited data). Suggests certification (data schema) as an alternative to one general standard. [2 - review in audio file] 

# Know-how / design contents
* Name
* Description
* Intended use
* Photograph
* Owner
* License
* Certification
* Graphical designs
 * Manafactured product
 * Parts
* Bill of Materials
* Assembly instructions
* Testing instructions
* Operating instructions
* Maintenance instructions
* Point of Work Risk Assessment

# Discussion points
## Know-how as something that can be exchanged
Know-How has been traditionally communicated in three ways [1]:
1. Master - apprentice model
2. Internal company structure & patenting 
3. Video - TV programs & Youtube

Is the objective enable the identification of know-how or making the know-how itself interoperable? 

## Level of prescription

* Data / file types for attribute information, documentation and tacit knowledge

## What comprises the "source code" 
* Discussion of what goes into the Github repository vs the wiki

## Does the requirement to be able to see a history, adapt, etc matter?

## Language
### "Thing" / "Piece of hardware" / "Product"
### 

# User Requirements
## End-User
Requirements that will be satisfied by the implementation of the standard.

### Must
*

### Should 
*

### Could
*

### Won't
*

## Standard-user
Requirements that will be satisfied within the standard itself in order to support implementation.

### Must
*

### Should 
*

### Could
*

### Won't
*

## Unsorted
* As a problem owner I want to identify designs that solve my problem without visiting each platform separately
* As a problem owner I want to identify makers who can make a design I have found
* As a designer I want my designs to be found by as many makers as possible
* As a designer I want to list my designs on several platforms without needing to rework the documentation
* As a designer I want to know the formats I should create my documentation in
* As a designer I want a specification for representing know-how in my documentation
* As a designer I want the freedom to produce my documentation in whatever format I think is most appropriate
* As a maker I want to know if a design is fully documented without having to dig through all the files
* As a maker I want to know if a design meets the open source definition
* As a commercial maker I want buyers to discover that I can make a thing from a design
* As a maker I want consistency in the format and presentation of design documentation
* As a maker I do not want to install proprietary or specialist software to view design and  documentation
* As a maker I do not want any minimum computer hardware specs to access designs and documentation 
* As a maker I need to access documentation over low bandwith internet connection
* As a hosting platform I want to add unique documentation features that solve my users' problems
* As an open hosting platform I want other platforms to be able to extract content from my platform and host it on their own 
* As an indexing platform I want to extract core information that helps users understand what designs represent and where they can find the documentation
* As an indexing platform I want to copy the documentation from other platforms onto my own
* As a translation platform I want to store the core information about a design in multiple languages
* As a translation platform I want to store the full documentation in multiple languages
* As a sponsor I want to know how many times designs I have sponsored have been made
* As a sponser I want to know who has made things from designs that I have sponsored 

# Proposal (working)
1. Define an agreed vision for operating model between designers, makers and platforms
2. Define a three level approach to Open Know-How that can be developed and adopted progressively
 1. Core meta-data for locating Open Hardware designs
 2. Schema for sharing the location of documentation
 3. Schema and formats for documentation
3. Information exchange mechanism that can be developed and adopted progressively by platforms
 1. Format of information containters
 2. Method of exchange


# References
[1] Open Know-How concept paper

[2] Open Know-How working group call 2019/02/21 
