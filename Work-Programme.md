* Open Know-How kick off call -  w/c 27th May 2019
* Evidence / resource gathering - 13th -31st May 2019 
* Draft scope circulated for comment - w/c 3rd June 2019
* Version 0.1 circulated for comment - w/c 24th June 2019
* Open Know-How Summit - Early July 2019
* Version 0.2 circulated for comment - w/c 5th August 2019
* Version 1.0 circulated for acceptance - w/c 19th August 2019
* Version 1.0 published - w/c 26th August 2019

